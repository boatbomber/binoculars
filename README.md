# Binoculars

**https://www.roblox.com/library/1962070654/Binoculars**

*The following is inside the code as well, but for redundancy and ease of use, it is here as well.*

**AUTHOR:** Zack

**CONTACT INFO:** @BoatbomberRBLX on Twitter, boatbomber on Roblox

**CREATION DATE:** 6/20/2018

**PURPOSE:** Creates a highly functioning binoculars feature for games

**SPECIAL NOTES:**	Place inside a local domain. List of possible domains:
	*ReplicatedFirst, StarterGui, StarterPack, StarterPlayer.StarterPlayerScripts, or StarterPlayer.StarterCharacterScripts*
	
# API

**Settings:**

 		EnableKey:
 			Description: The key that the binocular toggle is bound to
			Type: Enum.KeyCode
 			Example: Enum.KeyCode.B
			
 		HoldEnable:
 			Description: If true, player must hold down to keep binoculars on. If false, player must press once for on, once again for off.
			Type: Boolean
			Example: false
			
 		MaxZoom:
			Description: The most the binoculars can zoom in (How many times farther than if disabled)
			Type: Number
			Example: 15
			
		MinZoom:
			Description: The most the binoculars can zoom out (How many times farther than if disabled)
			Type: Number
 			Example: 2
			
			[NOTE: Due to the way the math works, you may end up with MaxZoom of 16.280549877368
				despite writing 15, and a MinZoom of 2.12378565 despite writing 2. These settings are
				for giving *roughly* how much zoom.]
			
 		AlwaysFirstPerson:
			Description: Determines if the binoculars return to 3rd Person view after disabling. If true, it will remain in 1st person view. If false, it will re-enable 3rd person view.
			Type: Boolean
 			Example: false